legacy-lifegame-python
=======================================

Python project for "Refactoring Legacy Code" workshop


SETUP
---------------------------------------

### with docker & docker-compose

```sh
$ docker-compose build
$ docker-compose up
```

entering CLI mode

```sh
docker-compose run --rm web bash
```

### local install

```sh
$ python3 -m venv venv
$ . venv/bin/activate
$ pip install -r requirements.txt
$ FLASK_ENV=development FLASK_APP=app.py flask run
```


DEMO
---------------------------------------

http://localhost:5000/lifegame/
