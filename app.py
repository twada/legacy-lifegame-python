from flask import Flask, request, url_for, render_template
from datetime import datetime
app = Flask(__name__)

@app.route('/lifegame/')
def lifegame():
    if request.args.get('prev') is not None:
        b = []
        for ln in request.args.get('prev').split('9'):
            r = []
            for c in list(ln):
                if c == '0':
                    r.append('□')
                else:
                    r.append('■')
            b.append(r)
        bb = []
        for i in range(5):
            bb.append([])
            for j in range(5):
                bb[i].append("□")
        for i in range(5):
            for j in range(5):
                # 誕生の場合
                if b[i][j] == "□":
                    count = 0
                    if i > 0 and j > 0 and b[i-1][j-1] == "■":
                        count = count+1
                    if i > 0 and b[i-1][j] == "■":
                        count = count+1
                    if i > 0 and j < 4 and b[i-1][j+1] == "■":
                        count = count+1
                    if j > 0 and b[i][j-1] == "■":
                        count = count+1
                    if j < 4 and b[i][j+1] == "■":
                        count = count+1
                    if i < 4 and j > 0 and b[i+1][j-1] == "■":
                        count = count+1
                    if i < 4 and b[i+1][j] == "■":
                        count = count+1
                    if i < 4 and j < 4 and b[i+1][j+1] == "■":
                        count = count+1
                    if count >= 3:
                        bb[i][j] = "■"
                    else:
                        bb[i][j] = "□"
                else:
                    bb[i][j] = "□"
                # 生存・過疎・過密の場合
                if b[i][j] == "■":
                    count = 0
                    if i > 0 and b[i-1][j] == "■":
                        count = count+1
                    if j > 0 and b[i][j-1] == "■":
                        count = count+1
                    if j < 4 and b[i][j+1] == "■":
                        count = count+1
                    if i < 4 and b[i+1][j] == "■":
                        count = count+1
                    if count == 2:
                        bb[i][j] = "■"
                    else:
                        bb[i][j] = "□"
        n = []
        for r in bb:
            nr = []
            for j in range(5):
                if r[j] == '□':
                    nr.append('0')
                else:
                    nr.append('1')
            n.append(''.join(nr))
        tstp = datetime.now().isoformat()
        return render_template('lifegame.html', b=bb, s='9'.join(n), tstp=tstp)
    else:
        b = []
        for i in range(5):
            b.append([])
            for j in range(5):
                b[i].append("□")
        b[2][1] = "■"
        b[2][2] = "■"
        b[2][3] = "■"
        n = []
        for r in b:
            nr = []
            for j in range(5):
                if r[j] == '□':
                    nr.append('0')
                else:
                    nr.append('1')
            n.append(''.join(nr))
        tstp = datetime.now().isoformat()
        return render_template('lifegame.html', b=b, s='9'.join(n), tstp=tstp)
